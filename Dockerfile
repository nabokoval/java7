# 12.04 is the latest LTS release
# see https://wiki.ubuntu.com/LTS
FROM ubuntu:12.04

# update
RUN apt-get update && apt-get install -y python-software-properties

RUN add-apt-repository ppa:webupd8team/java
RUN apt-get update

#install java
RUN echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
RUN apt-get install -y oracle-java7-installer

#remove installer
RUN rm -f /var/cache/oracle-jdk7-installer/*.tar.gz /usr/lib/jvm/java-7-oracle/src.zip
